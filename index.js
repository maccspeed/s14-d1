// Single Line Comments - Ctrl + /

/*
	Multi-line Comments - Ctrl + Shift + /
*/


/*	Syntax and Statements
	- STATEMENTS in programming are instructions that we tell the computer to perform.
	- javascript Statements usually end with semicolon(:).
	- SYNTAX in programming, it is the set of rules that describes how statemens must be constructed.
*/

console.log("Hello Batch 138!")

/*	Variables
	- is used to contain data.
	- declaring variables: tells our devices that a variable name is created and is ready to store data.
	- Syntax:
		let/const variableName;
		- let keyword is used to handle values that will change over time.
		- const [constant] keyword is used to handle values that are constant.
*/

let myVariable;

console.log(myVariable);

// console.log(hello);

// Initializing values to variables

let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;

console.log(productPrice);

/*	Reassigning variable values
	- change the initial or previous value into another value
*/

productName = 'Laptop';
console.log(productName);

const interest = 3.539;

/*interest = 4.489;
console.log(interest);*/

let supplier;

supplier = "John Smitch Tradings"

console.log(supplier);


// Declaring Multiple variables
let productCode = "DC017"
const productBrand = "Dell"

console.log(productCode, productBrand);

/*	Data Types
	- strings are series of characters that create a word, a phrase, a sentence or anything related to creating text.
	- strings in JavaScript can be writtend either a single (') or double (") quote.
*/

let country = 'Philippines';
let province = 'Metro Manila';

/*	String Concatenation
	Multiple string values can be combined to create a single string using the "+" symbol.
*/

let fullAddress = province + ", " + country;
console.log(fullAddress)
// Output: Metro Manila, Philippines

// Output: Welcome to Metro Manila, Philippines!
console.log("Welcome to " + fullAddress + "!");


/*	Escape character
	\n - refers to creating a new line in between text
*/
let mailAddress = "Metro Manila\n\nPhilippines"
console.log(mailAddress)

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numeric Data Types
// Integers/Whole numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers or Float numbers
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
// Output: John's grade last quarter is 98.7
console.log("John's grade last quarter is " + grade);

// Boolean - True / False, 1 / 0
let isMarried = true;
let isGoodConduct = false;
console.log("Married: " + isMarried);
console.log("Good Conduct: " + isGoodConduct);

//	Array - special kind of data type that is used to store multiple values
//	Arrays - can store different data types but is normally used to store similar data types.
/*	Syntax
	let/const arrayName = [element0, element1, element2, ...]
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);